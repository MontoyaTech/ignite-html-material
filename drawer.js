import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, div, h1, slot, button } from "../ignite-html/ignite-template.js";
import { IgniteProperty } from "../ignite-html/ignite-html.js";

class Drawer extends IgniteElement {
    constructor() {
        super();
    }

    get styles() {
        return /*css*/`
            mt-drawer {
                z-index: 100;
            }

            mt-drawer > .container {
                position: relative;
                background-color: #f8f9fa;
            }

            mt-drawer.absolute > .container {
                position: absolute;
            }

            mt-drawer.left {
                order: 0;
            }

            mt-drawer.left > .container {
                left: 0;
                top: 0;
                height: 100%;
            }

            mt-drawer.right {
                order: 9999;
            }

            mt-drawer.right > .container {
                right: 0;
                top: 0;
                height: 100%;
            }

            mt-drawer.top > .container {
                top: 0;
                left: 0;
                width: 100%;
            }

            mt-drawer.bottom > .container {
                bottom: 0;
                left: 0;
                width: 100%;
            }

            mt-drawer > .container {
                display: flex;
                flex-direction: column;
            }

            mt-drawer.mobile > .container {
                display: none;
                position: absolute;
                box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
            }

            mt-drawer.left > .open-btn {
                left: 1em;
                bottom: 1em;
            }

            mt-drawer.right > .open-btn {
                right: 1em;
                bottom: 1em;
            }

            mt-drawer > .open-btn {
                position: absolute;
                z-index: 100;
                width: 2.5em;
                height: 2.5em;
                font-size: 1.2em;
                background-color: white;
                border: none;
                outline: none !important;
                box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
                border-radius: 100%;
                display: flex;
                align-items: center;
                justify-content: center;
                text-align: center;
            }

            mt-drawer.left > .container > .close-btn {
                right: -3em;
                top: 0.5em;
            }

            mt-drawer.right > .container > .close-btn {
                left: -3em;
                top: 0.5em;
            }

            mt-drawer > .container > .close-btn {
                position: absolute;
                z-index: 100;
                width: 2.5em;
                height: 2.5em;
                font-size: 1.2em;
                background-color: white;
                border: none;
                outline: none !important;
                box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
                border-radius: 100%;
                display: flex;
                align-items: center;
                justify-content: center;
                text-align: center;
            }
        `;
    }

    get properties() {
        return {
            show: false,
            showToggleButton: true,
            responsive: new IgniteProperty(true, (oldValue, value) => {
                //If responsive is false and mobile is true, set mobile to false.
                if (!value && this.mobile) {
                    this.mobile = false;
                }

                this.update(); //If responsive is changed we must update.
            }),
            breakpoint: new IgniteProperty(768, () => {
                this.update(); //If responsive is changed we must update.
            }),
            mobile: false,
            placement: "left",
            width: "20em",
            height: null,
            padding: "1em",
            containerClass: null,
            absolute: true
        };
    }

    init() {
        this.resizeListener = () => this.update();

        window.addEventListener("resize", this.resizeListener);

        this.pushStateListener = () => this.onRouteChange();
        this.popStateListener = () => this.onRouteChange();

        window.addEventListener("popstate", this.popStateListener);
        window.addEventListener("pushstate", this.pushStateListener);

        //Run the first update
        this.update();
    }

    render() {
        return this.template
            .class(this.placement)
            .class(this.mobile, value => value ? "mobile" : null)
            .class(this.absolute, value => value ? "absolute" : null)
            .child(
                new button()
                    .class("open-btn")
                    .show([this.mobile, this.show, this.showToggleButton], (mobile, show, toggle) => mobile && !show && toggle)
                    .onClick(() => this.show = true)
                    .child("<i class='fas fa-chevron-right'></i>"),
                new div()
                    .style("width", this.width)
                    .style("height", this.height)
                    .style("padding", this.padding)
                    .style("display", this.show, false, value => value ? "flex" : "none")
                    .class(this.containerClass)
                    .class("container")
                    .child(
                        new button()
                            .class("close-btn")
                            .show([this.mobile, this.show, this.showToggleButton], (mobile, show, toggle) => mobile && show && toggle)
                            .onClick(() => this.show = false)
                            .child("<i class='fas fa-chevron-left'></i>")
                    )
                    .child(new slot(this))
            );
    }

    ready() {
        //Make sure we update in case responsive is changed.
        this.update();
    }

    update() {
        if (this.responsive) {
            if (window.innerWidth <= this.breakpoint && !this.mobile) {
                this.mobile = true;
            } else if (window.innerWidth > this.breakpoint && this.mobile) {
                this.mobile = false;
            }
        }
    }

    onRouteChange() {
        //If a route changed and we are mobile and being shown, hide ourself.
        if (this.mobile && this.show) {
            this.show = false;
        }
    }

    cleanup() {
        window.removeEventListener("resize", this.resizeListener);
        window.removeEventListener("popstate", this.popStateListener);
        window.removeEventListener("pushstate", this.pushStateListener);
    }
}

class DrawerTemplate extends IgniteTemplate {
    constructor(...children) {
        super("mt-drawer", children);
    }
}

export {
    DrawerTemplate as Drawer
};

customElements.define("mt-drawer", Drawer);