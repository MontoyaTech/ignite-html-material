import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, button, div, img, input } from "../ignite-html/ignite-template.js";

class EditableImage extends IgniteElement {
    constructor() {
        super();
    }

    get styles() {
        return /*css*/`
            mt-editable-image {
                position: relative;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
            }

            mt-editable-image > img {
                width: 100%;
                max-width: 100%;
                max-height: 100%;
                height: auto;
                flex: 1;
            }

            mt-editable-image.source:hover {
                cursor: pointer;
            }
        `;
    }

    get properties() {
        return {
            source: null,
            fileInput: null,
            objectFit: "contain",
            objectPosition: "center",
            width: null,
            maxWidth: null,
            height: null,
            maxHeight: null,
            placeholder: null
        };
    }

    render() {
        return this.template
            .onClick(() => this.onClick())
            .class(this.source, value => { return value != null ? "source" : null; })
            .child(
                new div()
                    .hide(this.source, value => { return value != null; })
                    .child(this.placeholder),
                new img()
                    .src(this.source)
                    .style("object-fit", this.objectFit)
                    .style("object-position", this.objectPosition)
                    .style("min-width", 0)
                    .style("min-height", 0)
                    .style("width", this.width)
                    .style("height", this.height)
                    .style("max-width", this.maxWidth)
                    .style("max-height", this.maxHeight)
                    .hide(this.source, value => { return value == null; })
                    .on("load", () => this.dispatchEvent(new Event("load"))),
                new input()
                    .type("file")
                    .style("display", "none", true)
                    .attribute("accept", "image/*")
                    .onChange((event) => this.onFileChange(event)).ref(this.fileInput)
            );
    }

    onFileChange(event) {
        //Prevent this event from bubbling up since we haven't
        //actually changed anything yet, just the inner input changed.
        event.stopPropagation();

        //Get the uploaded file from the file input.
        if (this.fileInput.files.length > 0) {
            var file = this.fileInput.files[0];

            //Create a reader to read in the file.
            var reader = new FileReader();
            reader.onload = (event) => {
                this.source = event.target.result;

                //Invoke a native change event now that we have
                //the image file ready.
                this.dispatchEvent(new Event("change"));
            };

            //Read it.
            reader.readAsDataURL(file);
        }
    }

    onClick() {
        this.fileInput.click();
    }
}

class EditableImageTemplate extends IgniteTemplate {
    constructor(...children) {
        super("mt-editable-image", children);
    }
}

customElements.define("mt-editable-image", EditableImage);

export {
    EditableImageTemplate as EditableImage
};