import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, div, h1, slot, button, list } from "../ignite-html/ignite-template.js";
import { IgniteProperty } from "../ignite-html/ignite-html.js";

class AppBar extends IgniteElement {
    constructor() {
        super();
    }

    get styles() {
        return /*css*/`
            mt-app-bar {
                position: relative;
                background-color: #fff;
                padding: 0.8em;
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: space-around;
                box-shadow: rgba(0, 0, 0, 0.2) 0px 3px 1px -2px, rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 1px 5px 0px;
            }

            mt-app-bar.bottom {
                order: 9999;
            }
        `;
    }

    get properties() {
        return {
            placement: "top"
        };
    }

    init() {

    }

    render() {
        return this.template
            .class(this.placement)
            .child(new slot(this));
    }

    ready() {

    }
}

class AppBarTemplate extends IgniteTemplate {
    constructor(...children) {
        super("mt-app-bar", children);
    }
}

export {
    AppBarTemplate as AppBar
};

customElements.define("mt-app-bar", AppBar);