import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, slot, list, div } from "../ignite-html/ignite-template.js";
import { IgniteProperty } from "../ignite-html/ignite-html.js";

class IconTabs extends IgniteElement {
    constructor() {
        super();
    }

    get styles() {
        return /*css*/`
            mt-icon-tabs > .icons {
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: center;
                gap: 1em;
            }
        `;
    }

    get properties() {
        return {
            icons: []
        };
    }

    render() {
        return this.template.child(
            new div().class("icons").child(
                new list(this.icons, icon => {
                    return new div(icon);
                })
            ),
            new slot(this)
        )
    }
}

class IconTabsTemplate extends IgniteTemplate {
    constructor(...children) {
        super("mt-icon-tabs", children);
    }
}

customElements.define("mt-icon-tabs", IconTabs);

export {
    IconTabsTemplate as IconTabs
};