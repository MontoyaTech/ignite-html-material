import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, button, ul, slot, span, div, list, html, pagination, population } from "../ignite-html/ignite-template.js";
import { IgniteProperty } from "../ignite-html/ignite-html.js";

class PaginationList extends IgniteElement {
    constructor() {
        super();
    }

    get properties() {
        return {
            pageSize: 10,
            currentPage: 0,
            items: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        };
    }

    get styles() {
        return /*css*/`
            mt-pagination-list .list-container {
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                gap: 1em;
                margin-bottom: 1em;
                align-items: center;
                justify-content: center;
            }

            mt-pagination-list .navigation-buttons button {
                background-color: #e0e0e0;
                border-color: #e0e0e0;
                border: 1px solid transparent;
                padding: .375rem .75rem;
                font-size: 1rem;
                border-radius: .25rem;
                color: #000;
                outline: none;
            }

            mt-pagination-list .navigation-buttons button.active {
                filter: brightness(75%);
            }

            mt-pagination-list .navigation-buttons {
                display: flex;
                flex-direction: row;
                gap: 0.5em;
                align-items: center;
            }
            
            mt-pagination-list .navigation-page-buttons {
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                align-items: center;
                justify-content: center;
                gap: 0.5em;
            }
        `;
    }

    render() {
        return this.template
            .style("display", "flex")
            .style("flex-direction", "column")
            .style("align-items", "center")
            .style("justify-content", "center")
            .onClick(e => e.stopPropagation())
            .child(
                new div()
                    .class("list-container")
                    .child(
                        new pagination(this.items, this.pageSize, this.currentPage, item => {
                            if (item instanceof IgniteTemplate) {
                                return item;
                            } else {
                                return new div(item);
                            }
                        })
                    ),
                new div().class("navigation-buttons").child(
                    new button().child("Prev").onClick(e => {
                        e.stopPropagation();
                        var pages = parseInt((this.items.length / this.pageSize)) + (this.items.length % this.pageSize > 0 ? 1 : 0);
                        if (this.currentPage >= pages) {
                            this.currentPage = pages - 1;
                        } else if (this.currentPage > 0) {
                            this.currentPage--;
                        }
                    }),
                    new div().class("navigation-page-buttons").child(
                        new population(
                            [this.pageSize, this.items],
                            index => {
                                return new button()
                                    .child((index + 1).toString())
                                    .class([this.currentPage, index], (currentPage, index) => {
                                        return currentPage == index ? "active" : null;
                                    })
                                    .onClick(e => {
                                        e.stopPropagation();
                                        this.currentPage = index;
                                    });
                            },
                            (pageSize, items) => {
                                var pages = parseInt((items.length / pageSize)) + (items.length % pageSize > 0 ? 1 : 0);

                                //Update our current page if we can and if we need to.
                                if (!(this.currentPage instanceof IgniteProperty) && this.currentPage >= pages) {
                                    this.currentPage = pages - 1;
                                }

                                return pages;
                            })
                    ),
                    new button().child("Next").onClick(e => {
                        e.stopPropagation();
                        var pages = parseInt((this.items.length / this.pageSize)) + (this.items.length % this.pageSize > 0 ? 1 : 0);
                        if (this.currentPage + 1 < pages) {
                            this.currentPage++;
                        }
                    })
                )
            )
    }
}

class PaginationListTemplate extends IgniteTemplate {
    constructor(...children) {
        super("mt-pagination-list", children);
    }
}

customElements.define("mt-pagination-list", PaginationList);

export {
    PaginationListTemplate as PaginationList
};