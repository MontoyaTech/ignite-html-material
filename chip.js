import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, slot, button, span } from "../ignite-html/ignite-template.js";

class Chip extends IgniteElement {
    constructor() {
        super();
    }

    get properties() {
        return {
            onDelete: () => { },
            background: null,
            color: null,
            readOnly: false
        }
    }

    get styles() {
        return /*css*/`
            mt-chip {
                border-radius: 1em;
                background-color: #e0e0e0;
                padding-top: 0.3em;
                padding-bottom: 0.3em;
                padding-left: 0.6em;
                padding-right: 0.6em;
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: center;
            }

            mt-chip:hover {
                filter: brightness(0.9);
            }
        `;
    }

    render() {
        return this.template
            .style("background", this.background)
            .style("color", this.color)
            .child(
                new slot(this),
                new button()
                    .class("btn ml-2 p-0")
                    .child(`<i class="fal fa-times"></i>`)
                    .style("color", this.color)
                    .style("line-height", "1")
                    .style("border", "none")
                    .onClick(() => this.onDelete())
                    .hide(this.readOnly)
            );
    }
}

class ChipTemplate extends IgniteTemplate {
    constructor(...children) {
        super("mt-chip", children);
    }
}

customElements.define("mt-chip", Chip);

export {
    ChipTemplate as Chip
}