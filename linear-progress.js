import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, slot, div, svg, circle } from "../ignite-html/ignite-template.js";

class LinearProgress extends IgniteElement {
    constructor() {
        super();
    }

    get styles() {
        return /*css*/`
            mt-linear-progress {
                display: flex;
                align-items: center;
                justify-content: center;
            }
            
            mt-linear-progress > div {
                height: 0.3em;
                width: 100%;
                position: relative;
                overflow: hidden;
                background-color: #ddd;
            }
            
            mt-linear-progress > div > div {
                display: block;
                position: absolute;
                content: "";
                left: -30em;
                width: 30em;
                height: 0.3em;
                animation: loading 2s linear infinite;
            }
            
            @keyframes loading {
                from {
                    left: -30em;
                    width: 0%;
                }
            
                50% {
                    width: 30%;
                }
            
                70% {
                    width: 70%;
                }
            
                80% {
                    left: 50%;
                }
            
                95% {
                    left: 120%;
                }
            
                to {
                    left: 100%;
                }
            }
        `;
    }

    get properties() {
        return {
            loading: true,
            color: "#26B3FC"
        };
    }

    render() {
        return this.template.child(
            new div(
                new div().style("background-color", this.color)
            )
        ).show(this.loading)
    }
}

class LinearProgressTemplate extends IgniteTemplate {
    constructor(...children) {
        super("mt-linear-progress", children);
    }
}

customElements.define("mt-linear-progress", LinearProgress);

export {
    LinearProgressTemplate as LinearProgress
}