import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, slot, div, svg, circle } from "../ignite-html/ignite-template.js";

class CircularProgress extends IgniteElement {
    constructor() {
        super();
    }

    get styles() {
        return /*css*/`
        mt-circular-progress {
            display: flex;
            flex-direction: column;
        }
        
        mt-circular-progress > div {
            position: relative;
            flex: 1;
        }

        mt-circular-progress > div > svg {
            z-index: 1;
            animation: rotator 2s linear infinite;
        }
          
        mt-circular-progress > div > svg > circle {
            stroke-dasharray: 80px, 200px;
            stroke-dashoffset: 0px;
            animation: dash 2s ease-in-out infinite;
        }

        @keyframes rotator {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        @keyframes dash {
            0% {
                stroke-dasharray: 1px, 200px;
                stroke-dashoffset: 0px;
            }
            50% {
                stroke-dasharray: 100px, 200px;
                stroke-dashoffset: -15px;
            }
            100% {
                stroke-dasharray: 100px, 200px;
                stroke-dashoffset: -125px;
            }
        }
        `;
    }

    get properties() {
        return {
            loading: true,
            size: "4em",
            strokeWidth: "3.6",
            stroke: "#4285F4",
            center: true
        };
    }

    render() {
        return this.template.child(
            new div().child(
                new svg()
                    .attribute("viewBox", "22 22 44 44")
                    .style("width", this.size)
                    .style("height", this.size)
                    .style("left", [this.size, this.center], null, (size, center) => center ? `calc(50% - (${size} / 2))` : null)
                    .style("top", [this.size, this.center], null, (size, center) => center ? `calc(50% - (${size} / 2))` : null)
                    .style("position", this.center, true, center => center ? "absolute" : "relative")
                    .show(this.loading)
                    .child(
                        new circle()
                            .attribute("cx", "44")
                            .attribute("cy", "44")
                            .attribute("r", "20.2")
                            .attribute("fill", "none")
                            .attribute("stroke-width", this.strokeWidth)
                            .attribute("stroke", this.stroke)
                    ),
                new slot(this)
            )
        )
    }
}

class CircularProgressTemplate extends IgniteTemplate {
    constructor(...children) {
        super("mt-circular-progress", children);
    }
}

customElements.define("mt-circular-progress", CircularProgress);

export {
    CircularProgressTemplate as CircularProgress
}