import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteTemplate, div, slot } from "../ignite-html/ignite-template.js";

class CollapsableRegion extends IgniteElement {
    constructor() {
        super();
    }

    get styles() {
        return /*css*/`
            mt-collapsable-region .title:hover {
                cursor: pointer;
            }
        `;
    }

    get properties() {
        return {
            collapse: true,
            title: "Placeholder"
        };
    }

    render() {
        return this.template.child(
            new div()
                .class("title")
                .onClick(() => this.collapse = !this.collapse)
                .child(this.title),
            new div().hide(this.collapse).child(
                new slot(this)
            )
        );
    }
}

class CollapsableRegionTemplate extends IgniteTemplate {
    constructor(...children) {
        super("mt-collapsable-region", children);
    }
}

customElements.define("mt-collapsable-region", CollapsableRegion);

export {
    CollapsableRegionTemplate as CollapsableRegion
};